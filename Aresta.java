

public class Aresta {

	private int peso;
	private String nomeAresta;
	private int v1;
	private int v2;
	
	public Aresta(int v1,int v2,int peso, String nomeAresta) {
		this.v1 = v1;
		this.v2 = v2;
		this.peso = peso;
		this.nomeAresta = nomeAresta;
	}
	public Aresta(int peso, String nomeAresta) {
		this.peso = peso;
		this.nomeAresta = nomeAresta;
	}
	public int getPeso() {
		return peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public String getNomeAresta() {
		return nomeAresta;
	}

	public void setNomeAresta(String nomeAresta) {
		this.nomeAresta = nomeAresta;
	}

	public int getV1() {
		return v1;
	}

	public void setV1(int v1) {
		this.v1 = v1;
	}

	public int getV2() {
		return v2;
	}

	public void setV2(int v2) {
		this.v2 = v2;
	}

}
