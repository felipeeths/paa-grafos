

public class Algoritimos {

	private Grafo grafo;
	// Variaveis busca em largura
	public byte branco = 0;
	public byte cinza = 1;
	public byte preto = 2;

	private int d[], antecessor[];

	// Variaveis Busca profundidade
	private int t[];

	// Variaveis para Dijktra
	private double p[];

	// Kruskal
	public int parent[] = new int[9];

	public Algoritimos(Grafo grafo) {
		this.grafo = grafo;
		int n = this.grafo.getNumVertices();
		this.d = new int[n];
		this.antecessor = new int[n];
		this.t = new int[n];

	}

	// Busca em Profundidade a partir de um dado vértice de origem
	public void dfs() {

		int tempo = 0;
		int cor[] = new int[this.grafo.getNumVertices()];
		for (int u = 0; u < grafo.getNumVertices(); u++) {
			cor[u] = branco;
			this.antecessor[u] = -1;
		}
		for (int u = 0; u < grafo.getNumVertices(); u++)
			if (cor[u] == branco)
				tempo = this.visitaDfs(u, tempo, cor);

	}

	// Busca em Largura a partir de um dado vértice de origem
	public void bfs() throws Exception {
		int cor[] = new int[this.grafo.getNumVertices()];
		for (int u = 0; u < grafo.getNumVertices(); u++) {
			cor[u] = branco;
			this.d[u] = Integer.MAX_VALUE;
			this.antecessor[u] = -1;
		}
		for (int u = 0; u < grafo.getNumVertices(); u++)
			if (cor[u] == branco)
				this.visitaBfs(u, cor);
	}

	// Dijkstra: dado um vértice de origem calcular os menores caminhos para os
	// demais vértices
	public void dijkstra(int raiz) throws Exception {
		int n = this.grafo.getNumVertices();
		this.p = new double[n]; // @{\it peso dos v\'ertices}@
		int vs[] = new int[n + 1]; // @{\it v\'ertices}@
		this.antecessor = new int[n];
		for (int u = 0; u < n; u++) {
			this.antecessor[u] = -1;
			p[u] = Double.MAX_VALUE; // @$\infty$@
			vs[u + 1] = u; // @{\it Heap indireto a ser constru\'{\i}do}@
		}
		p[raiz] = 0;
		ExtrairMin heap = new ExtrairMin(p, vs);
		heap.constroi();
		while (!heap.vazio()) {
			int u = heap.retiraMin();
			if (!this.grafo.listaAdjVazia(u)) {
				Aresta adj = grafo.primeiroListaAdj(u);
				while (adj != null) {
					int v = adj.getV2();
					if (this.p[v] > (this.p[u] + adj.getPeso())) {
						antecessor[v] = u;
						heap.diminuiChave(v, this.p[u] + adj.getPeso());
					}
					adj = grafo.proxAdj(u);
				}
			}
		}
	}

	// Bellman-Ford: dado um vértice de origem calcular os menores caminhos para
	// os demais vértices
	public void ford() {

	}

	// Kruskal: apresentar uma Árvore Geradora Mı́nima
	public void kruskal() {
		int a = 0, b = 0, u = 0, v = 0;
		Aresta kru[][] = grafo.getAresta();
		int mincost = 0, num = 0;
		for (int i = 0; i < grafo.getNumVertices(); i++) {
			for (int j = 0; j < grafo.getNumVertices(); j++) {
				if (kru[i][j].getPeso() == 0) {
					kru[i][j].setPeso(999);
				}
			}
		}
		int i, min;
		while (num < grafo.getNumVertices()) {
			for (i = 0, min = 999; i < grafo.getNumVertices(); i++) {
				for (int j = 0; j < grafo.getNumVertices(); j++) {
					if (kru[i][j].getPeso() < min) {
						min = kru[i][j].getPeso();
						a = u = i;
						b = v = j;
					}
				}
			}
			u = acha(u);
			v = acha(v);
			if (union(u, v) == 0) {
				if (min != 999) {
					System.out.println("("+ a + "," + b + ")" +  "=" +min);
					num++;
					mincost += min;
				} else {
					break;
				}

			}
			kru[b][a].setPeso(999);
			kru[a][b].setPeso(999);

		}
		System.out.println("Custo Minimo = " + mincost);

	}

	/*
	 * Algoritimos complementares para o principal
	 */
	private void visitaBfs(int u, int cor[]) throws Exception {
		cor[u] = cinza;
		this.d[u] = 0;
		Fila fila = new Fila();
		fila.enfileira(new Integer(u));
		while (!fila.vazia()) {
			Integer aux = (Integer) fila.desenfileira();
			u = aux.intValue();
			if (!this.grafo.listaAdjVazia(u)) {
				Aresta a = this.grafo.primeiroListaAdj(u);
				while (a != null) {
					int v = a.getV2();
					if (cor[v] == branco) {
						cor[v] = cinza;
						this.d[v] = this.d[u] + 1;
						this.antecessor[v] = u;
						fila.enfileira(new Integer(v));
					}
					a = this.grafo.proxAdj(u);
				}
			}
			cor[u] = preto;
		}
	}

	private int visitaDfs(int u, int tempo, int cor[]) {
		cor[u] = cinza;
		this.d[u] = ++tempo;
		// System.out.println ("Visita "+u+" Descoberta:"+this.d[u]+" cinza");
		if (!this.grafo.listaAdjVazia(u)) {
			Aresta a = this.grafo.primeiroListaAdj(u);
			while (a != null) {
				int v = a.getV2();
				if (cor[v] == branco) {
					this.antecessor[v] = u;
					tempo = this.visitaDfs(v, tempo, cor);
				}
				a = this.grafo.proxAdj(u);
			}
		}
		cor[u] = preto;
		this.t[u] = ++tempo;
		// System.out.println ("Visita " +u+ " Termino:" +this.t[u]+ " preto");
		return tempo;
	}
	//Achar verteces
	int acha(int i) {
		if(parent[i] != i)
			i = parent[i];
		return i;
	}
	//Verifica a união
	int union(int i, int j) {
		if (i != j) {
			parent[j] = i;
			return 1;
		}
		return 0;
	}

	public int getD(int v) {
		return this.d[v];
	}

	public int getAntecessor(int v) {
		return this.antecessor[v];
	}

	public int getT(int v) {
		return this.t[v];
	}

	public double peso(int u) {
		return this.p[u];
	}
}
