
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;

/*
 * Faz carregamento de um grafo em um arquivo e monta o grafico
 *  
 */
public class CarregarGrafo {

	private BufferedReader linhas;

	public Grafo abrirArquivo() throws IOException {
		File arquivo = null;
		JFileChooser j = new JFileChooser();
		if (j.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			arquivo = j.getSelectedFile();
		}
		linhas = new BufferedReader(new FileReader(arquivo));
		String linha = linhas.readLine();
		String orientado[] = linha.split("=");
		linha = null;
		linha = linhas.readLine();
		linha = linha.replace("{", "");
		linha = linha.replace("}", "");
		String nomeV[] = linha.split("=");
		String nomeSeparados[] = nomeV[1].split(",");
		if (nomeSeparados.length == 1) {
			try {
				int numero = Integer.parseInt(nomeSeparados[0]);
				String novoNome[] = new String[numero];
				for (int i = 0; i < numero; i++) {
					novoNome[i] = Integer.toString(i);
					// System.out.println(novoNome[i]);
				}
				nomeSeparados = novoNome;
			} catch (Exception e) {

			}
		}

		boolean orientadosn;
		String orientadonao = orientado[1];

		if (orientadonao.contains("n")) {
			orientadosn = false;
		} else {
			orientadosn = true;
		}
		int numVertices = nomeSeparados.length;
		Grafo grafo = new Grafo(numVertices, orientadosn, nomeSeparados);

		while (linhas.ready()) {
			linha = null;
			linha = linhas.readLine();
			String arestas2[] = linha.split(":");
			String aresta1 = arestas2[0].replace("(", "");
			aresta1 = aresta1.replace(")", "");
			String aresta[] = aresta1.split(",");
			int Vertice1 = Integer.parseInt(aresta[0]);
			int Vertice2 = Integer.parseInt(aresta[1]);
			String peso_nome[] = arestas2[1].split(",");
			int peso = Integer.parseInt(peso_nome[0]);
			String nomeAresta;
			if (peso_nome.length == 1) {
				nomeAresta = "";
			} else {
				nomeAresta = peso_nome[1];
			}
			if (orientadosn == false) {
				grafo.insereAresta(Vertice1, Vertice2, peso, nomeAresta);
				grafo.insereAresta(Vertice2, Vertice1, peso, nomeAresta);
			} else {
				grafo.insereAresta(Vertice1, Vertice2, peso, nomeAresta);
			}

		}

		return grafo;

	}
}
