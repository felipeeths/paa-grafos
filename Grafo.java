

/*
 * Implementção do Grafo Matriz de Adjacências
 */
public class Grafo {

	private int numVertices;
	private boolean orientado;
	private String nomeVert[];
	private int pos[];
	private Aresta aresta[][];

	//Incializa o matriz grafo com 0 e nome vazio e alguns atributos do grafo.
	public Grafo(int numVertices, boolean orientado, String nomeVert[]) {

		this.aresta = new Aresta[numVertices][numVertices];
		this.pos = new int[numVertices];
		this.numVertices = numVertices;
		this.orientado = orientado;
		this.nomeVert = nomeVert;

		for (int i = 0; i < this.numVertices; i++) {
			for (int j = 0; j < this.numVertices; j++) {
				this.aresta[i][j] = new Aresta(0, "");
			}
			this.pos[i] = -1;
		}
	}

	//insere aresta com o peso correspondente e o nome
	public void insereAresta(int v1, int v2, int peso, String nomeAresta) {
		this.aresta[v1][v2].setV1(v1);
		this.aresta[v1][v2].setV2(v2);
		this.aresta[v1][v2].setPeso(peso);
		this.aresta[v1][v2].setNomeAresta(nomeAresta);
	}

	//Imprime o grafo em forma de Matriz Adjacências
	public void imprime() {
		System.out.print("   ");
		for (int i = 0; i < this.numVertices; i++)
			System.out.print(i + "   ");
		System.out.println();
		for (int i = 0; i < this.numVertices; i++) {
			System.out.print(i + "  ");
			for (int j = 0; j < this.numVertices; j++) {

				if (this.aresta[i][j].getPeso() != 0) {
					System.out.print(1 + "   ");
				} else {
					System.out.print(0 + "   ");
				}
				// System.out.print (this.aresta[i][j].peso + " ");
				// System.out.print (this.aresta[i][j].nomeAresta + " ");
			}
			System.out.println();
		}
	}
	// Verifica se existe algum vertice adjacente
	public boolean listaAdjVazia(int v) {
		for (int i = 0; i < this.numVertices; i++)
			if (this.aresta[v][i].getPeso() != 0)
				return false;
		return true;
	}
	
	//Retorna a primeira aresta que o vertice v participa ou null caso contrario
	public Aresta primeiroListaAdj(int v) {
		this.pos[v] = -1;
		return this.proxAdj(v);
	}

    //Retorna a proxima aresta que o vertice v participa ou null caso contrario 
	public Aresta proxAdj(int v) {
		this.pos[v]++;
		while ((this.pos[v] < this.numVertices) && (this.aresta[v][this.pos[v]].getPeso() == 0))
			this.pos[v]++;
		if (this.pos[v] == this.numVertices)
			return null;
		else
			return aresta[v][this.pos[v]];
	}

	public int getNumVertices() {
		return numVertices;
	}

	public void setNumVertices(int numVertices) {
		this.numVertices = numVertices;
	}

	public boolean isOrientado() {
		return orientado;
	}

	public void setOrientado(boolean orientado) {
		this.orientado = orientado;
	}

	public String[] getNomeVert() {
		return nomeVert;
	}

	public void setNomeVert(String[] nomeVert) {
		this.nomeVert = nomeVert;
	}

	public int[] getPos() {
		return pos;
	}

	public void setPos(int[] pos) {
		this.pos = pos;
	}

	public Aresta[][] getAresta() {
		return aresta;
	}

	public void setAresta(Aresta[][] aresta) {
		this.aresta = aresta;
	}
}
