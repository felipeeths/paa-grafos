

import java.util.Scanner;

/*
 * Arquivo main menu interface console.
 */
public class Main {

	public static void main(String[] args) {
		Scanner ler = new Scanner(System.in);
		CarregarGrafo carrega;
		carrega = new CarregarGrafo();
		Grafo grafo = null;
		int op;
		do {
			System.out.println("----------------------------------------");
			System.out.println("Trabalho PAA - Algoritimos Grafos");
			System.out.println("1 - Carregar Grafo");
			System.out.println("2 - Mostrar Adjacencia");
			System.out.println("3 - Mostrar Grafo");
			System.out.println("4 - Busca Profundidade");
			System.out.println("5 - Busca Largura");
			System.out.println("6 - Dijkstra");
			System.out.println("7 - Bellman-Ford(Não implementado)");
			System.out.println("8 - Kruskal");
			System.out.println("0 - Sair");
			// System.out.println("\033[0m BLACK");
			// System.out.println("\033[31m RED");
			// System.out.println("\033[32m GREEN");
			// System.out.println("\033[33m YELLOW");
			// System.out.println("\033[34m BLUE");
			// System.out.println("\033[35m MAGENTA");
			// System.out.println("\033[36m CYAN");
			// System.out.println("\033[37m WHITE");
			System.out.print("Opção: ");
			op = ler.nextInt();
			switch (op) {
			case 1:
				try {
					grafo = carrega.abrirArquivo();
					System.out.println("Arquivo Carregado ");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 2:
				System.out.println("----- Buscar lista adjacencia ----");
				System.out.print("Vertice: ");
				int v1 = ler.nextInt();
				if (!grafo.listaAdjVazia(v1)) {
					Aresta adj = grafo.primeiroListaAdj(v1);
					while (adj != null) {
						String nome[] = grafo.getNomeVert();
						System.out
								.println(nome[adj.getV2()] + " - " + adj.getNomeAresta() + " (" + adj.getPeso() + ")");
						adj = grafo.proxAdj(v1);
					}
					System.out.println();
				}
				break;
			case 3:
				System.out.println("--------- Grafo --------");
				grafo.imprime();
				break;
			case 4:
				System.out.println("--------- Busca Profundidade --------");

				Algoritimos prof = new Algoritimos(grafo);
				prof.dfs();
				for (int v = 0; v < grafo.getNumVertices(); v++) {
					System.out.println("d[" + v + "]:" + prof.getD(v) + " -- t[" + v + "]:" + prof.getT(v)
							+ " -- antecessor[" + v + "]:" + prof.getAntecessor(v));
				}

				break;
			case 5:
				System.out.println("--------- Busca Largura --------");

				Algoritimos larg = new Algoritimos(grafo);
				try {
					larg.bfs();
					// larg.imprimeCaminho(origem, grafo.getNumVertices() - 1);
					for (int v = 0; v < grafo.getNumVertices(); v++) {
						System.out.println(
								"d[" + v + "]:" + larg.getD(v) + " -- antecessor[" + v + "]:" + larg.getAntecessor(v));
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 6:
				System.out.println("--------- Dijkstra --------");
				System.out.print("Vertice de Origem: ");
				int origem;
				origem = ler.nextInt();
				Algoritimos dij = new Algoritimos(grafo);
				try {
					dij.dijkstra(origem);
					for (int v = 0; v < grafo.getNumVertices(); v++)
						if (dij.getAntecessor(v) != -1)
							System.out.println("(" + dij.getAntecessor(v) + "," + v + ") -- Peso:" + (int)dij.peso(v));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case 7:
				System.out.println("--------- Ford --------");
				break;
			case 8:
				System.out.println("--------- Kruskal --------");
				Algoritimos krus = new Algoritimos(grafo);
				krus.kruskal();
				break;
			case 0:
				Sair();
			}
		} while (op != 0);
		ler.close();

	}

	public static int Sair() {
		System.out.println("Saindo ...");
		return 0;
	}
}
